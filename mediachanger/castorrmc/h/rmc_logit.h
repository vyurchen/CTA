/*
 * Copyright (C) 2001 by CERN/IT/PDP/DM
 * All rights reserved
 */

#pragma once

#include "osdep.h"

EXTERN_C int rmc_logit(const char *const func, const char *const msg, ...);

