/*
 * Copyright (C) 1998-2002 by CERN/IT/PDP/DM
 * All rights reserved
 */

#pragma once

#include "osdep.h"

EXTERN_C int rmc_sendrep(const int rpfd, const int rep_type, ...);

