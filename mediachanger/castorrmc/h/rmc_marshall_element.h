/*
 * Copyright (C) 2001 by CERN/IT/PDP/DM
 * All rights reserved
 */

#pragma once

#include "osdep.h"
#include "smc_struct.h"

EXTERN_C int rmc_marshall_element (char **const sbpp, const struct smc_element_info *const element_info);

