#!/bin/sh

VID=L80013

FILES=`nslisttape -sV $VID | cut -f1 -d' '`

mt rewind
mt fsf 1
for i in `seq 1 $FILES`
do
  echo -n "VID: $VID, FILE: $i, DATE: "
  date
  /root/tests/LTO-8/physical-position.sh
  mt fsf 3
done
