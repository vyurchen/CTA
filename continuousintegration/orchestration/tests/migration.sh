#!/bin/bash

usage() { cat <<EOF 1>&2
Usage: $0 -n <namespace>
EOF
exit 1
}

while getopts "n:" o; do
    case "${o}" in
        n)
            NAMESPACE=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${NAMESPACE}" ]; then
    usage
fi

if [ ! -z "${error}" ]; then
    echo -e "ERROR:\n${error}"
    exit 1
fi

echo "Preparing namespace for the tests"
./prepare_tests.sh -n ${NAMESPACE}
if [ $? -ne 0 ]; then
  echo "ERROR: failed to prepare namespace for the tests"
  exit 1
fi

echo
echo "Launching grpc_dir_inject.sh on ctaeos pod"
kubectl -n ${NAMESPACE} cp grpc_dir_inject.sh ctaeos:/root/grpc_dir_inject.sh
kubectl -n ${NAMESPACE} exec ctaeos -- bash /root/grpc_dir_inject.sh || exit 1

exit 0
