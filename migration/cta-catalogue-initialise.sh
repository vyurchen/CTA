#!/bin/sh
#
# Initialise CTA Catalogue on first use

# LHC experiments
cta-admin virtualorganization add --vo "ALICE"   --comment "ALICE"
cta-admin virtualorganization add --vo "ATLAS"   --comment "ATLAS"
cta-admin virtualorganization add --vo "CMS"     --comment "CMS"
cta-admin virtualorganization add --vo "LHCb"    --comment "LHCb"

# SME experiments on Namespace dashboard
cta-admin virtualorganization add --vo "COMPASS" --comment "COMPASS"
cta-admin virtualorganization add --vo "NA64"    --comment "NA64"
cta-admin virtualorganization add --vo "AMS"     --comment "AMS"
cta-admin virtualorganization add --vo "nTOF"    --comment "nTOF"

# Other SME experiments
cta-admin virtualorganization add --vo "NA62"    --comment "NA62"

# Media types as discovered from CASTOR (17/06/2020)
#   SQL> select unique density from vmgr_tape_info;
#
#   DENSITY
#   --------
#   20TC
#   15TC
#   9TC
#   12TC
#   7000GC

cta-admin mediatype add \
    --name T10K500G  \
    --capacity 500000000000 \
    --primarydensitycode 74 \
    --cartridge "T10000" \
    --comment "Oracle T10000 cartridge formated at 500 GB (for developers only)"
cta-admin mediatype add \
    --name 3592JC7T \
    --capacity 7000000000000 \
    --primarydensitycode 84 \
    --cartridge "3592JC" \
    --comment "IBM 3592JC cartridge formated at 7 TB"
cta-admin mediatype add \
    --name 3592JD15T \
    --capacity 15000000000000 \
    --primarydensitycode 85 \
    --cartridge "3592JD" \
    --comment "IBM 3592JD cartridge formated at 15 TB"
cta-admin mediatype add \
    --name 3592JE20T \
    --capacity 20000000000000 \
    --primarydensitycode 87 \
    --cartridge "3592JE" \
    --comment "IBM 3592JE cartridge formated at 20 TB"
cta-admin mediatype add \
    --name LTO7M \
    --capacity 9000000000000 \
    --primarydensitycode 93 \
    --cartridge "LTO-7" \
    --comment "LTO-7 M8 cartridge formated at 9 TB"
cta-admin mediatype add \
    --name LTO8 \
    --capacity 12000000000000 \
    --primarydensitycode 94 \
    --cartridge "LTO-8" \
    --comment "LTO-8 cartridge formated at 12 TB"

