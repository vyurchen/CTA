/*****************************************************************************
 *              castorvmgr_ctamigration_schema.sql
 *
 * This file is part of the Castor/CTA project.
 * See http://cern.ch/castor and http://cern.ch/eoscta
 * Copyright (C) 2019  CERN
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * This script adds the necessary grants to an existing CASTOR VMGR schema
 * in order to support the metadata migration to CTA, the CASTOR successor.
 *
 * @author Castor Dev team, castor-dev@cern.ch
 *****************************************************************************/

UNDEF ctaSchema
ACCEPT ctaSchema CHAR PROMPT 'Enter the username of the CTA schema: ';

GRANT SELECT ON Vmgr_tape_side TO &ctaSchema;
GRANT SELECT ON Vmgr_tape_info TO &ctaSchema;
GRANT SELECT ON Vmgr_tape_dgnmap TO &ctaSchema;

UNDEF cnsSchema
ACCEPT cnsSchema CHAR PROMPT 'Enter the username of the CASTOR Nameserver schema: ';

GRANT UPDATE ON Vmgr_tape_side TO &cnsSchema;

