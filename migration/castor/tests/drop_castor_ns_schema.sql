drop table CASTORCONFIG;
drop table CNS_TP_POOL;
drop table CNS_USER_METADATA;
drop table CNS_SYMLINKS;
drop table CNS_FILES_EXIST_TMP;
drop table CNS_SEG_METADATA;
drop table CNS_FILE_METADATA;
drop table CNS_CLASS_METADATA;
drop table SetSegsForFilesInputHelper;
drop table SetSegsForFilesResultsHelper;
drop table Dirs_Full_Path;

drop sequence Cns_unique_id;
drop function getTime;
drop procedure cns_ctaLog;
