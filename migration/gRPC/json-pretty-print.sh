#!/bin/sh
#
# Pretty Print JSON with Base64 decoding of byte-encoded fields

if [ $# -eq 1 ]
then
  cat $1
else
  cat -
fi | sed 's/\]\[/,/g' | python -mjson.tool |\
awk '/"path": / || /"name": / ||
     /"value": / ||
     /"CTA_.*": / ||
     /"sys\..*": / ||
     /"eos.btime": / {
       CODE=substr($2, 2)
       sub(/",*$/,"",CODE)
       command="echo "CODE"|base64 -d"
       command | getline DECODED
       close(command)
       sub(CODE,DECODED)
   }
   { print $0 }'
