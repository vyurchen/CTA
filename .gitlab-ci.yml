stages:
  - build:srpm
  - build:rpm
  - build:dockerimage
  - rename:dockerimage
  - test
  - regressions
  - shouldfail
  - publish_rpm

before_script:
  - export CTA_BUILD_ID=${CI_PIPELINE_ID}git${CI_COMMIT_SHA:0:8}
  - echo "Exporting CTA_BUILD_ID=${CTA_BUILD_ID}"
  - test -n "${CI_COMMIT_TAG}" && export TAG_VERSION=$(echo ${CI_COMMIT_TAG} | sed -e 's/^v//;s/-.*$//')
  - test -n "${CI_COMMIT_TAG}" && export TAG_RELEASE=$(echo ${CI_COMMIT_TAG} | sed -e 's/^[^-]*-//')
  - echo "Removing protectbase from all repos (same as Puppet)"
  - sed -i '/^protect=/d' /etc/yum.repos.d/* 

cta_srpm:
  stage: build:srpm
  retry: 1
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - yum install -y gcc-c++ cmake make rpm-build
    - mkdir build_srpm
    - cd build_srpm
    - cmake -DPackageOnly:Bool=true -DVCS_VERSION=${CTA_BUILD_ID} ..
    - make cta_srpm

  artifacts:
    expire_in: 30 days
    paths:
    - build_srpm/RPM/SRPMS/

  tags:
    - docker

cta_rpm:
  except:
    - tags
  stage: build:rpm
  retry: 1
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum.repos.d/*.repo /etc/yum.repos.d/
    - yum install -y gcc-c++ cmake make rpm-build
    - yum -y install yum-plugin-priorities yum-plugin-versionlock
    - yum install -y git
    - git submodule update --init --recursive
    - cd xrootd-ssi-protobuf-interface && export XROOTD_SSI_PROTOBUF_INTERFACE_VERSION=$(git describe --abbrev=0) && cd ..
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum/pluginconf.d/versionlock.list /etc/yum/pluginconf.d/
    - yum-builddep --nogpgcheck -y build_srpm/RPM/SRPMS/*
    - mkdir build_rpm
    - cd build_rpm
    - cmake -DVCS_VERSION=${CTA_BUILD_ID} ..
    - make cta_rpm

  artifacts:
    expire_in: 30 days
    paths:
    - build_rpm/RPM/RPMS

  tags:
    - docker

cta_tagged_rpm:
  only:
    - tags
  stage: build:rpm
  retry: 1
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - if [ -z "${CI_COMMIT_TAG}" ]; then echo "This is not a tagged commit, exiting"; exit 0; fi
    - echo "Building package for tag ${CI_COMMIT_TAG}. CTA_VERSION=${TAG_VERSION} - CTA_RELEASE=${TAG_RELEASE}"
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum.repos.d/*.repo /etc/yum.repos.d/
    - yum install -y gcc-c++ cmake make rpm-build
    - yum -y install yum-plugin-priorities yum-plugin-versionlock
    - yum install -y git
    - git submodule update --init --recursive
    - cd xrootd-ssi-protobuf-interface && export XROOTD_SSI_PROTOBUF_INTERFACE_VERSION=$(git describe --abbrev=0) && cd ..
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum/pluginconf.d/versionlock.list /etc/yum/pluginconf.d/
    - yum-builddep --nogpgcheck -y build_srpm/RPM/SRPMS/*
    - mkdir build_tagged_rpm
    - cd build_tagged_rpm
    - CTA_VERSION=${TAG_VERSION} cmake -DVCS_VERSION=${TAG_RELEASE} ..
    - make cta_rpm

  artifacts:
    expire_in: 30 days
    paths:
    - build_tagged_rpm/RPM/RPMS

  tags:
    - docker

publish_tagged_rpm:
  stage: publish_rpm
  only:
    - tags
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - if [ -z "${CI_COMMIT_TAG}" ]; then echo "This is not a tagged commit, exiting"; exit 0; fi
    - EOS_ACCOUNT_USERNAME=${DOCKER_LOGIN_USERNAME} EOS_ACCOUNT_PASSWORD=${DOCKER_LOGIN_PASSWORD} CI_OUTPUT_DIR=build_tagged_rpm/RPM/RPMS EOS_PATH=/eos/user/c/ctareg/www/cta-repo HOOK=/eos/user/c/ctareg/www/cta-repo/update_repos.sh continuousintegration/ci_helpers/deploy-eos.sh
  tags:
    - docker

cta_valgrind:
  except:
    - tags
  stage: test
  retry: 1
  variables:
    CTAREPODIR: /tmp/repo
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum.repos.d/* /etc/yum.repos.d/
    - yum -y install yum-plugin-priorities createrepo
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum/pluginconf.d/versionlock.list /etc/yum/pluginconf.d/
    - mkdir -p ${CTAREPODIR}; cp -r build_rpm/RPM ${CTAREPODIR}
    - createrepo ${CTAREPODIR}; echo -e "[cta-artifacts]\nname=CTA artifacts\nbaseurl=file://${CTAREPODIR}\ngpgcheck=0\nenabled=1\npriority=2" > /etc/yum.repos.d/cta-artifacts.repo
    - yum -y --nogpgcheck install cta-systemtests cta-debuginfo sqlite-debuginfo --enablerepo=debug
    - /usr/bin/cta-unitPlusSystemTests.sh

  tags:
    - docker

dbunittests_postgresql:
  except:
    - tags
  stage: test
  variables:
    CTAREPODIR: /tmp/repo
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum.repos.d/* /etc/yum.repos.d/
    - yum -y install yum-plugin-priorities createrepo
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum/pluginconf.d/versionlock.list /etc/yum/pluginconf.d/
    - mkdir -p ${CTAREPODIR}; cp -r build_rpm/RPM ${CTAREPODIR}
    - createrepo ${CTAREPODIR}; echo -e "[cta-artifacts]\nname=CTA artifacts\nbaseurl=file://${CTAREPODIR}\ngpgcheck=0\nenabled=1\npriority=2" > /etc/yum.repos.d/cta-artifacts.repo
    - yum -y --nogpgcheck install cta-systemtests cta-debuginfo sqlite-debuginfo --enablerepo=debug
    - yum -y install cta-catalogueutils
    - yum -y install rh-postgresql12-postgresql-server
    - POSTGRESQL_DATA_DIR=/usr/local/cta_test_pgsql_data
    - POSTGRESQL_LOG_DIR=/var/log/postgres
    - echo POSTGRESQL_DATA_DIR=${POSTGRESQL_DATA_DIR}
    - echo POSTGRESQL_LOG_DIR=${POSTGRESQL_LOG_DIR}
    - mkdir -p ${POSTGRESQL_DATA_DIR}
    - chown -R postgres:postgres ${POSTGRESQL_DATA_DIR}
    - mkdir -p ${POSTGRESQL_LOG_DIR}
    - chown -R postgres:postgres ${POSTGRESQL_LOG_DIR}
    - export LD_LIBRARY_PATH=/opt/rh/rh-postgresql12/root/usr/lib64
    - POSTGRES_BIN=/opt/rh/rh-postgresql12/root/usr/bin
    - echo POSTGRES_BIN=${POSTGRES_BIN}
    - runuser -u postgres -- ${POSTGRES_BIN}/initdb -D ${POSTGRESQL_DATA_DIR}
    - runuser -u postgres -- ${POSTGRES_BIN}/pg_ctl start -w -t 10 -D ${POSTGRESQL_DATA_DIR} -l ${POSTGRESQL_LOG_DIR}/cta_test_postgres.log
    - runuser -u postgres -- ${POSTGRES_BIN}/createdb cta
    - runuser -u postgres -- ${POSTGRES_BIN}/createuser -E cta
    - CTA_CATALOGUE_CONF=/etc/cta/cta-catalogue.conf
    - echo CTA_CATALOGUE_CONF=${CTA_CATALOGUE_CONF}
    - echo 'postgresql:postgresql://cta@localhost/cta' > ${CTA_CATALOGUE_CONF}
    - /usr/bin/cta-catalogue-schema-create ${CTA_CATALOGUE_CONF}
    - /usr/bin/cta-rdbmsUnitTests ${CTA_CATALOGUE_CONF}
    - valgrind /usr/bin/cta-rdbmsUnitTests ${CTA_CATALOGUE_CONF}
    - runuser -u postgres -- ${POSTGRES_BIN}/pg_ctl stop -D ${POSTGRESQL_DATA_DIR}

  tags:
    - docker

dbunittests_mysql:
  except:
    - tags
  stage: test
  variables:
    CTAREPODIR: /tmp/repo
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  script:
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum.repos.d/* /etc/yum.repos.d/
    - yum -y install yum-plugin-priorities createrepo
    - cp -f continuousintegration/docker/ctafrontend/cc7/etc/yum/pluginconf.d/versionlock.list /etc/yum/pluginconf.d/
    - mkdir -p ${CTAREPODIR}; cp -r build_rpm/RPM ${CTAREPODIR}
    - createrepo ${CTAREPODIR}; echo -e "[cta-artifacts]\nname=CTA artifacts\nbaseurl=file://${CTAREPODIR}\ngpgcheck=0\nenabled=1\npriority=2" > /etc/yum.repos.d/cta-artifacts.repo
    - yum -y --nogpgcheck install cta-systemtests cta-debuginfo sqlite-debuginfo --enablerepo=debug
    - yum -y install cta-catalogueutils
    - yum -y install lsof rh-mariadb103-mariadb-server
    - echo '[mariadb]' > /etc/opt/rh/rh-mariadb103/my.cnf.d/cta_server_listen.cnf
    - echo 'bind-address=localhost' >> /etc/opt/rh/rh-mariadb103/my.cnf.d/cta_server_listen.cnf
    - scl enable rh-mariadb103 -- mysql_install_db --user=mysql
    - cd '/opt/rh/rh-mariadb103/root/usr'
    - scl enable rh-mariadb103 -- /opt/rh/rh-mariadb103/root/usr/bin/mysqld_safe --user=mysql --datadir=/var/opt/rh/rh-mariadb103/lib/mysql < /dev/null &
    - echo "Sleeping 5 seconds"
    - sleep 5
    - echo "Finished sleeping"
    - echo 'create database cta;' | scl enable rh-mariadb103 -- mysql
    - echo 'create user cta@localhost;' | scl enable rh-mariadb103 -- mysql
    - echo 'grant all privileges on *.* to cta@localhost' | scl enable rh-mariadb103 -- mysql
    - echo 'flush privileges;' | scl enable rh-mariadb103 -- mysql
    - CTA_CATALOGUE_CONF=/etc/cta/cta-catalogue.conf
    - echo CTA_CATALOGUE_CONF=${CTA_CATALOGUE_CONF}
    - echo 'mysql://cta@localhost:3306/cta' > ${CTA_CATALOGUE_CONF}
    - /usr/bin/cta-catalogue-schema-create ${CTA_CATALOGUE_CONF}
    - /usr/bin/cta-rdbmsUnitTests ${CTA_CATALOGUE_CONF}
    - valgrind /usr/bin/cta-rdbmsUnitTests ${CTA_CATALOGUE_CONF}
    - kill `ps -ef | egrep '^mysql.*/usr/libexec/mysqld' | awk '{print $2;}'`

  tags:
    - docker

ctageneric_docker:
  except:
    - tags
  stage: build:dockerimage
  retry: 1
  tags:
    - docker-image-build
  script:
    - ""
  variables:
    TO: gitlab-registry.cern.ch/cta/ctageneric:${CI_PIPELINE_ID}
    DOCKER_FILE: continuousintegration/docker/ctafrontend/cc7/ci_runner/Dockerfile

ctageneric_docker_rename:
  except:
    - tags
  stage: rename:dockerimage
  variables:
    TO: gitlab-registry.cern.ch/cta/ctageneric
    REPOSITORY: cta/ctageneric
    GITLAB_HOST: gitlab.cern.ch
  script:
    - export OLDTAG=${CI_PIPELINE_ID}
    - export NEWTAG=${CTA_BUILD_ID}
    - cd continuousintegration/ci_helpers/
    - ./rename_tag.sh

archiveretrieve:
  except:
    - tags
  stage: test
  script:
    - export NAMESPACE="archiveretrieve-${CTA_BUILD_ID}-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1)"
    - cd continuousintegration/orchestration/; ./run_systemtest.sh -n ${NAMESPACE} -p ${CI_PIPELINE_ID} -s tests/archive_retrieve.sh -O -D -t 2400 -C
  artifacts:
    when: always
    expire_in: 30 days
    paths:
    - pod_logs
  tags:
    - kubernetes
    - mhvtl
    
repack:
  except:
    - tags
  stage: test
  script:
    - export NAMESPACE="repack-${CTA_BUILD_ID}-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1)"
    - cd continuousintegration/orchestration/; ./run_systemtest.sh -n ${NAMESPACE} -p ${CI_PIPELINE_ID} -s tests/repack_systemtest_wrapper.sh -O -D -t 2400 -C
  artifacts:
    when: always
    expire_in: 30 days
    paths:
    - pod_logs
  tags:
    - kubernetes
    - mhvtl

dbunittests_oracle:
  except:
    - tags
  stage: test
  script:
    - export NAMESPACE="dbunittestsoracle-${CTA_BUILD_ID}-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1)"
    - cd continuousintegration/orchestration/; ./run_systemtest.sh -n ${NAMESPACE} -p ${CI_PIPELINE_ID} -s /usr/bin/true -O -D -U -t 600 -C
  artifacts:
    when: always
    expire_in: 30 days
    paths:
    - pod_logs
  tags:
    - kubernetes

nightly1:
  stage: shouldfail
  only:
    - triggers
  script:
    - echo "Running nightly tests"
    - export NAMESPACE="archiveretrieve-${CTA_BUILD_ID}-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1)"
    - cd continuousintegration/orchestration/; ./run_systemtest.sh -n ${NAMESPACE} -p ${CI_PIPELINE_ID} -s tests/test_nightly1.sh -O -D
  artifacts:
    when: always
    expire_in: 30 days
    paths:
    - pod_logs
  tags:
    - kubernetes
    - mhvtl
    - xlarge

regressioneos:
  stage: regressions
  only:
    - triggers
    - schedules
  script:
    - echo "Running nightly eos regression tests"
    - export NAMESPACE="archiveretrieve-${CTA_BUILD_ID}-$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 4 | head -n 1)"
    - cd continuousintegration/orchestration/; ./run_systemtest.sh -n ${NAMESPACE} -p ${CI_PIPELINE_ID} -s tests/upgrade-eos.sh -O -D -S
  artifacts:
    when: always
    expire_in: 30 days
    paths:
    - pod_logs
  tags:
    - kubernetes
    - mhvtl
    - xlarge
